# Tarkett Aladdin Chrome Extension

Source code of the Aladdin Chrome Extension

1. [Installation](./docs/install.md)
1. [Develop](./docs/dev.md)
1. [Deploy](./docs/deploy.md)
1. [Publish](./docs/publish/publish.md)

## How it works

1. [Architecture](./docs/architecture.md)
1. [User Journey](./docs/user-journey/user-journey.md)
1. **DEPRECATED** - [Pinterest login](./docs/pinterest/pinterest.md)
