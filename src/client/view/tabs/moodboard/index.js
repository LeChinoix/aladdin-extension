import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Moodboard from './Moodboard.vue'
import {
  MdButton,
  MdDialog,
  MdDialogConfirm,
  MdEmptyState,
  MdIcon,
  MdSpeedDial,
  MdToolbar,
  MdTooltip
} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'shared/style/vue-style.scss'
import I18n from 'shared/services/I18n'
import { PAGE_NAMES } from 'shared/constants'

Vue.config.productionTip = false

Vue.use(MdButton)
Vue.use(MdDialog)
Vue.use(MdDialogConfirm)
Vue.use(MdEmptyState)
Vue.use(MdIcon)
Vue.use(MdSpeedDial)
Vue.use(MdToolbar)
Vue.use(MdTooltip)
Vue.use(VueI18n)

const { createVuei18n } = new I18n(PAGE_NAMES.MOODBOARD)
const i18n = createVuei18n()

/* eslint-disable no-new */
new Vue({
  el: '#root',
  i18n,
  render: h => h(Moodboard)
})
