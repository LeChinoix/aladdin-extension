import JSZip from 'jszip'
import FileSaver from 'file-saver'
import ScreenshotRepository from 'shared/repositories/ScreenshotRepository'
import { getFileExtensionFromDataURL, getContentFromDataURL } from 'utils/dataURL'

class SceenshotDownloader {
  zipDataURLs = async (dataURLs, filebaseName) => {
    var zip = new JSZip()
    dataURLs.forEach((dataURL, index) => {
      const fileExtension = getFileExtensionFromDataURL(dataURL)
      zip.file(
        `${filebaseName}-${index}.${fileExtension}`,
        getContentFromDataURL(dataURL),
        { base64: true }
      )
    })
    const content = await zip.generateAsync({type: 'blob'})
    return content
  }

  createMoodboardZip = async () => {
    const { getListScreenshots } = new ScreenshotRepository()
    const screenshots = await getListScreenshots()
    const screenshotDataUrls = screenshots.map((screenshot) => screenshot.url)
    const zipBlob = await this.zipDataURLs(screenshotDataUrls, 'image')
    FileSaver.saveAs(zipBlob, 'moodboard.zip')
  }
}

export default SceenshotDownloader
