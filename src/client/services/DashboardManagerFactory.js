import Draggabilly from 'draggabilly'
import Packery from 'packery'

class DashboardManager extends Packery {
  refreshItems = () => {
    this.reloadItems()
    this.layout()
  }

  bindDragEventsToItem = (item) => {
    const draggie = new Draggabilly(item)
    this.bindDraggabillyEvents(draggie)
  }
}

class DashboardManagerFactory {
  createDashboardManager = (element, itemClass) => {
    return new DashboardManager(
      element,
      {
        itemSelector: `.${itemClass}`,
        columnWidth: `.${itemClass}`,
        gutter: 5,
        percentPosition: true
      }
    )
  }
}

export default DashboardManagerFactory
