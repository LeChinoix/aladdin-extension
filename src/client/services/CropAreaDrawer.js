import ChromeMessageEventManager from 'shared/ChromeMessageEventManager'
import messageTypes from 'shared/messageTypes'
import { getWindowInnerDimensions } from 'utils/window'

class CropAreaDrawer {
  constructor () {
    const chromeMessageEventManager = new ChromeMessageEventManager()
    chromeMessageEventManager.addListener(messageTypes.SEARCH_WITH_SCREENSHOT, this.resetCoordinates)
    chromeMessageEventManager.addListener(messageTypes.ADD_TO_MOODBOARD, this.resetCoordinates)
  }

  resetCoordinates = () => {
    this.endX = 0
    this.endY = 0
    this.startX = 0
    this.startY = 0
  }

  startCropper = (e) => {
    this.startX = this.endX = e.clientX
    this.startY = this.endY = e.clientY
  }

  updateCropAreaPosition = (e) => {
    this.endY = e.clientY
    this.endX = e.clientX
  }

  getCropAreaCoordinates = () => {
    const { endX, endY, startX, startY } = this
    const minX = Math.min(endX, startX)
    const maxX = Math.max(endX, startX)
    const minY = Math.min(endY, startY)
    const maxY = Math.max(endY, startY)

    return {
      topLeft: { x: minX, y: minY },
      topRight: { x: maxX, y: minY },
      bottomRight: { x: maxX, y: maxY },
      bottomLeft: { x: minX, y: maxY }
    }
  }

  getCropAreaDistanceToBorder = () => {
    let top
    let right
    let bottom
    let left

    const windowDimensions = getWindowInnerDimensions()

    const { endX, endY, startX, startY } = this

    // Calculating the values differently depending on how the user start's dragging.
    if (endX >= startX) {
      right = windowDimensions.width - endX
      left = startX
    } else {
      right = windowDimensions.width - startX
      left = endX
    }

    if (endY >= startY) {
      top = startY
      bottom = windowDimensions.height - endY
    } else {
      top = endY
      bottom = windowDimensions.height - startY
    }

    return {
      top,
      right,
      bottom,
      left
    }
  }
}

export default CropAreaDrawer
