import messageTypes from 'shared/messageTypes'
import ChromeMessageEventManager from 'shared/ChromeMessageEventManager'
import { getWindowFullDimensions } from 'utils/window'

class CropAreaMessages {
  takeScreenshot = (cropAreaCoordinates) => {
    const chromeMessageEventManager = new ChromeMessageEventManager()
    chromeMessageEventManager.sendMessageToBackground(
      messageTypes.SEARCH_WITH_SCREENSHOT,
      {
        cropAreaCoordinates,
        windowWidth: getWindowFullDimensions().width
      }
    )
  }

  addToMoodboard = (cropAreaCoordinates) => {
    const chromeMessageEventManager = new ChromeMessageEventManager()
    chromeMessageEventManager.sendMessageToBackground(
      messageTypes.ADD_TO_MOODBOARD,
      {
        cropAreaCoordinates,
        windowWidth: getWindowFullDimensions().width
      }
    )
  }
}

export default CropAreaMessages
