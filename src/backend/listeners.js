import ScreenshoterMessage from 'shared/messages/ScreenshoterMessage'
import ChromeMessageEventManager from 'shared/ChromeMessageEventManager'
import ImageHandler from 'shared/services/ImageHandler'
import TabManager from 'shared/services/TabManager'
import CropAreaHandler from './services/CropAreaHandler'
import RedirectionHandler from './services/RedirectionHandler'
import { addListener as addContextMenuListener } from './contextMenu'
import messageTypes from 'shared/messageTypes'
import { createDataURLFromImageURL } from 'utils/dataURL'

const addContextMenuListeners = () => {
  const {
    ADD_TO_MOODBOARD,
    DISPLAY_SCREENSHOTER,
    OPEN_MOODBOARD,
    SEARCH_WITH_IMAGE
  } = messageTypes
  const { displayScreenshoter } = new ScreenshoterMessage()

  addContextMenuListener(DISPLAY_SCREENSHOTER, displayScreenshoter)
  addContextMenuListener(OPEN_MOODBOARD, TabManager.openOrFocusMoodboardTab)
  addContextMenuListener(ADD_TO_MOODBOARD, async ({ srcUrl }) => {
    const dataURL = await createDataURLFromImageURL(srcUrl)
    ImageHandler.addToMoodboard(dataURL)
  })
  addContextMenuListener(SEARCH_WITH_IMAGE, async ({ srcUrl }) => {
    const dataURL = await createDataURLFromImageURL(srcUrl)
    ImageHandler.searchWithImage(dataURL)
  })
}

const addChromeMessageListeners = () => {
  const { ADD_TO_MOODBOARD, REDIRECT_TO_TAB, SEARCH_WITH_SCREENSHOT } = messageTypes
  const { addToMoodboard, searchWithScreenshot } = new CropAreaHandler()
  const { redirectToTab } = new RedirectionHandler()
  const { addListener: addChromeMessageListener } = new ChromeMessageEventManager()

  addChromeMessageListener(SEARCH_WITH_SCREENSHOT, searchWithScreenshot)
  addChromeMessageListener(ADD_TO_MOODBOARD, addToMoodboard)
  addChromeMessageListener(REDIRECT_TO_TAB, redirectToTab)
}

export default () => {
  addContextMenuListeners()
  addChromeMessageListeners()
}
