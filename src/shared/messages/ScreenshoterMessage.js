import ChromeMessageEventManager from 'shared/ChromeMessageEventManager'
import messageTypes from 'shared/messageTypes'

class ScreenshoterMessage {
  displayScreenshoter = ({ tabId }) => {
    const chromeMessageEventManager = new ChromeMessageEventManager()
    chromeMessageEventManager.sendMessageToTab(tabId, messageTypes.DISPLAY_SCREENSHOTER)
  }
}

export default ScreenshoterMessage
