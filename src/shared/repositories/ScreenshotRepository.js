import Storage from '.'
import { createImageElementFromURL, getImageProportions } from 'utils/image'

class ScreenshotRepository extends Storage {
  constructor () {
    super('screenshots')
  }

  createScreenshot = async (dataURL) => {
    const isLandscape = await this.getIsLandscapeFromURL(dataURL)
    return {
      url: dataURL,
      isLandscape
    }
  }

  getIsLandscapeFromURL = async (imageUrl) => {
    const imageElement = await createImageElementFromURL(imageUrl)
    return getImageProportions(imageElement) > 16 / 9
  }

  getListScreenshots = () => this.getListValues()

  addScreenshot = async (imgURL) => {
    const screenshot = await this.createScreenshot(imgURL)
    this.updateValue(
      this.indexById(screenshot)
    )
  }
}

export default ScreenshotRepository
