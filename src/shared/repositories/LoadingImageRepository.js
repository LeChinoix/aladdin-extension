import Storage from '.'

class LoadingImageRepository extends Storage {
  constructor () {
    super('loadingImage')
  }

  getLoadingImageURL = () => this.getAll()

  setLoadingImageURL = async (url) => {
    this.setValue(url)
  }
}

export default LoadingImageRepository
