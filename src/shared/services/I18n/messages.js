import { PAGE_NAMES } from 'shared/constants'

export default {
  [PAGE_NAMES.MOODBOARD]: {
    en: {
      title: 'Click on an image to search a floor associated on Tarkett',
      printTitle: 'Your moodboard',
      confirmDeletion: {
        title: 'Delete all your moodboard\'s pictures?',
        info: 'This operation cannot be canceled.',
        confirm: 'Confirm',
        cancel: 'Cancel'
      },
      button: {
        tooltip: {
          print: 'Print board',
          delete: 'Empty board',
          download: 'Download board'
        }
      },
      placeholder: {
        title: 'Need inspiration?',
        description: 'Add new images to your moodboard',
        button: 'Go to Tarkett\'s Pinterest'
      }
    },
    fr: {
      title: 'Cliquez sur une image pour faire une recherche de sol Tarkett',
      printTitle: 'Votre moodboard',
      confirmDeletion: {
        title: 'Supprimer toutes les images du moodboard ?',
        info: 'Cette opération ne peut pas être annulée.',
        confirm: 'Confirmer',
        cancel: 'Annuler'
      },
      button: {
        tooltip: {
          print: 'Imprimer le board',
          delete: 'Vider votre board',
          download: 'Télécharger votre board'
        }
      },
      placeholder: {
        title: 'Besoin d\'inspiration ?',
        description: 'Ajoutez de nouvelles images à votre Moodboard',
        button: 'Aller sur le Pinterest de Tarkett'
      }
    }
  },
  [PAGE_NAMES.LOADING_PAGE]: {
    en: {
      failed: {
        message: 'Oops the search failed... You can try again or come back later'
      },
      progress: {
        message: 'Searching your floor, this can take a several seconds...'
      }
    },
    fr: {
      failed: {
        message: 'Oops la recherche a échoué... Vous pouvez essayer de la relancer ou revenir plus tard'
      },
      progress: {
        message: 'Recherche en cours, cela peut prendre plusieurs secondes...'
      }
    }
  },
  [PAGE_NAMES.POPUP]: {
    en: {
      captureScreenshot: 'Capture screeshot',
      goToMoodboard: 'Go to moodboard'
    },
    fr: {
      captureScreenshot: 'Faire une capture',
      goToMoodboard: 'Aller au moodboard'
    }
  },
  [PAGE_NAMES.CONTENT_SCRIPT]: {
    en: {
      helper: 'Select an area on your screen to search a similar floor on Tarkett website',
      button: {
        tooltip: {
          addToMoodboard: 'Add to moodboard',
          redo: 'Cancel selection',
          search: 'Search on Tarkett site'
        }
      }
    },
    fr: {
      helper: 'Sélectionnez une zone de votre écran pour rechercher un sol semblable sur le site de Tarkett',
      button: {
        tooltip: {
          addToMoodboard: 'Ajouter au moodboard',
          redo: 'Annuler la sélection',
          search: 'Lancer une recherche sur Tarkett'
        }
      }
    }
  },
  [PAGE_NAMES.CONTEXT_MENU]: {
    en: {
      captureScreenshot: 'Capture screenshot',
      goToMoodboard: 'Go to moodboard',
      addImageToMoodboard: 'Add this image to moodboard',
      searchImage: 'Search this image'
    },
    fr: {
      captureScreenshot: 'Faire une capture',
      goToMoodboard: 'Aller au moodboard',
      addImageToMoodboard: 'Ajouter cette image au moodboard',
      searchImage: 'Rechercher avec cette image'
    }
  },
  [PAGE_NAMES.BROWSER_ACTION]: {
    en: {
      title: 'Find your floor'
    },
    fr: {
      title: 'Trouvez votre sol'
    }
  }
}
