import { createImageElementFromURL } from 'utils/image'

class ImageCropper {
  constructor (windowWidth) {
    this.windowWidth = windowWidth
  }

  applyCropOnImage = (imageElement, cropArea) => {
    const qualityFactor = imageElement.width / this.windowWidth
    const croppedImageWidth = cropArea.width * qualityFactor
    const croppedImageHeight = cropArea.height * qualityFactor
    const canvas = document.createElement('canvas')
    canvas.width = croppedImageWidth
    canvas.height = croppedImageHeight
    canvas.getContext('2d').drawImage(
      imageElement,
      cropArea.start.x * qualityFactor,
      cropArea.start.y * qualityFactor,
      croppedImageWidth,
      croppedImageHeight,
      0,
      0,
      croppedImageWidth,
      croppedImageHeight
    )
    return canvas.toDataURL()
  }

  cropImage = async (fullImageURL, cropAreaCoordinates) => {
    const cropArea = {
      width: cropAreaCoordinates.topRight.x - cropAreaCoordinates.topLeft.x,
      height: cropAreaCoordinates.bottomLeft.y - cropAreaCoordinates.topLeft.y,
      start: cropAreaCoordinates.topLeft
    }
    const imageElement = await createImageElementFromURL(fullImageURL)
    const croppedImageURL = await this.applyCropOnImage(imageElement, cropArea)
    return croppedImageURL
  }
}

export default ImageCropper
