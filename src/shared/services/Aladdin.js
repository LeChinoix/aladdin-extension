import axios from 'axios'
import { getBlobFromDataURL } from 'utils/dataURL'

class Aladdin {
  searchProductsURL = 'https://professionnels.tarkett.fr/fr_FR/rechercher/produits'
  aladdinURL = 'https://professionnels.tarkett.fr/fr_FR/search-by-image'

  createSearchProductsUrl = (names, skuIds) => (
    `${this.searchProductsURL}?` +
    `skuIds=${skuIds.join('+')}&` +
    `originalName=${names.originalName}&` +
    `bucketName=${names.bucketName}`
  )

  searchAladdinWithImageURL = async (imgURL) => {
    const imgBlob = getBlobFromDataURL(imgURL)
    const serpURL = await this.searchAladdinWithImageBlob(imgBlob)
    return serpURL
  }

  searchAladdinWithImageBlob = async (imgBlob) => {
    const { names, skuIds } = await this.getProductsFromPicture(imgBlob)
    const url = this.createSearchProductsUrl(names, skuIds)
    return url
  }

  getProductsFromPicture = async (resizedBlob) => {
    const formData = new FormData()
    const extension = resizedBlob.type.split('/')[1]
    formData.append('picture', resizedBlob, `screenshot.${extension}`)
    const { data: searchResult } = await axios.post(this.aladdinURL, formData)
    return searchResult
  }
}

export default Aladdin
