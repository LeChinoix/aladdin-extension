import TabManager from 'shared/services/TabManager'
import ScreenshotRepository from 'shared/repositories/ScreenshotRepository'
import LoadingImageRepository from 'shared/repositories/LoadingImageRepository'

class ImageHandler {
  static addToMoodboard = async (imageURL) => {
    const screenshotRepository = new ScreenshotRepository()
    await screenshotRepository.addScreenshot(imageURL)
    TabManager.openOrFocusMoodboardTab()
  }

  static searchWithImage = async (imageURL) => {
    const loadingImageRepository = new LoadingImageRepository()
    await loadingImageRepository.setLoadingImageURL(imageURL)
    TabManager.openLoadingPageTab()
  }
}

export default ImageHandler
