# User journey

![User Journey](./aladdin-user-journey.png)

## Zoom on the main journey

Here is a zoom with technical steps on the red highlighted journey:

![Sequence Diagram](./search-sequence-diagram.svg)

_Generated on https://mermaidjs.github.io/mermaid-live-editor_
