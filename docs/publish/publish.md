# Publish a new version of Aladdin Chrome Extension

To publish a new version of Aladdin Chrome Extension:

Credentials:

- Login: __tarkett.aladdin@gmail.com__
- Password: __Tarkett2019__

Modify the Extension information:

- Go to the [Chrome dashboard](https://chrome.google.com/webstore/developer/dashboard).
- Login with __tarkett.aladdin@gmail.com__
- Click on "modify" in Aladdin line

## Chrome Store validation

You will not have access to your dashboard / publicated extension until the Chrome Store team validate the new build.

⚠️ **Modifying the publication information cancel the previous validation process (if not finished)**

⏱ Validation takes around 2 - 4 days

## Publish a new version

- Click on "import updated package"
- Select your new zipped build
- Validate

## Update extension information

You can modify any information on the page.
It will be used by Google to update the CHrome Store page and to promote the extension.

⚠️ Right now, the item is available only in France

__To change this:__ Change the selected items in the _Region_ section

![the region section](./region-dashboard.png)

⚠️ Right now, the item is not listed in Chrome webstore

__To change this:__ Change the visibility to __Public__ in the _Visibility_ section

![the visibility section](./visibility-dashboard.png)

⚠️ Right now, the item is available only in English

__To change this:__ Change the selected language to the desired language in the _Language_ section

![the language section](./language-dashboard.png)

## Internationalization

⚠️ __Need changes made by a developer__

To make the Chrome extension available in multiple languages, you will have to modify your `manifest.json` file and to internationalize your information in the `_locales/fr/messages.json` file.

[The Chrome documentation on internationalization](https://developer.chrome.com/webstore/i18n?hl=en#details)
