# Pinterest Authentication

⚠️ __DEPRECATED__

To interact with Pinterest, we have to create an authentication token.
To achieve this, we use the oauth endpoints provided by Pinterest.

## Ideal Pinterest Auth

![Normal OAuth](./ideal-pinterest-auth.png)

## Actual Pinterest Auth

For security reason, __a webpage cannot redirect or link to a Chrome Extension Tab__.

The workaround to login anyway is to use another webpage watched by the contentScript.
This page is a Github Pages, the source code is hosted on the [`gh-pages` branch of this repository](https://github.com/the-ultimate-flooring-experience/aladdin-chrome-extension/tree/gh-pages).

Here is the diagram of what actually happens:

![Actual OAuth](./actual-pinterest-auth.png)

⚠️️️️️️️️️️️️️️️️️️️️️️ **Warning !**
️
The code sent back by Pinterest as query Param is a One Time Token.
If you want to generate another token, you can not reuse it.
